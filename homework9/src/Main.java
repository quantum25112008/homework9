import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите оценку A, B, C, D, F");
        String grade = scanner.nextLine();

        switch(grade){
            case "A":
                System.out.println("Ваш ребёнок учится на отлично");
                break;
            case "B":
                System.out.println("Ваш ребёнок учится хорошо");
                break;
            case "C":
                System.out.println("Ваш ребёнок старается учистя");
                break;
            case "D":
                System.out.println("Ваш ребёнок учистя");
                break;
            case "F":
                System.out.println("Ваш не ребёнок не учистя");
                break;
            default:
                System.out.println("Вы выбрали не существующию оценку");
        }
    }
}